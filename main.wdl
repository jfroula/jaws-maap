workflow jgi_maap {
    File input_file  
    Int threads
    File data

    call rqcfilt {
      input: infile=input_file,
             threads=threads
    }

    call bfc {
      input: infile=rqcfilt.filtered,
             threads=threads
    }

    call split {
      input: infile=bfc.out
    }

    call spades {
      input: read1=split.read1,
             read2=split.read2
    }

    call fungalrelease {
      input: infile=spades.scaffolds
    }

    call stats {
      input: infile=fungalrelease.assy
    }

    call bbmap {
      input: fastq=rqcfilt.filtered,
             ref=fungalrelease.assy
    }
    call prodigal {
        input: contigs=spades.scaffolds
    }
    call hmmer {
        input: inproteins=prodigal.faa,
                      data=data
    }
    call cmsearch {
        input: contigs=spades.scaffolds,
                   data=data
    }
}

# Error correct and filter junk from reads
task rqcfilt {
    File infile
    Int threads

    command {
        shifter --image=bryce911/bbtools:38.44 rqcfilter.sh -Xmx50G threads=${threads} in=${infile} \
           out=filtered.fastq.gz \
           path=. trimfragadapter=t qtrim=r trimq=10 maxns=0 maq=12 minlen=51 mlf=0.33 \
           phix=t removehuman=t removedog=t removecat=t removemouse=t khist=t removemicrobes=f sketch=f kapa=t clumpify=t
    }
    output {
        File filtered = "filtered.fastq.gz"
    }
}

task bfc {
    File infile 
    Int threads
    
    command
    {
        echo -e "\n### running bfc infile: ${infile} ###\n" 
        shifter --image=jfroula/bfc:181  \
            bfc -1 -k 25 -b 32 -t ${threads} ${infile} 1> bfc.fastq 2> bfc.error 

        shifter --image=jfroula/jgi_meta_annot:1.1.2 \
            seqtk trimfq bfc.fastq 1> bfc.seqtk.fastq 2> seqtk.error
        
        shifter --image=jfroula/jgi_meta_annot:1.1.2 \
            pigz -c bfc.seqtk.fastq -p 4 -2 1> filtered.bfc.fq.gz 2> pigz.error 
    }
    output {
        File out = "filtered.bfc.fq.gz"
    }
}

# split paired end reads for spades (and generate some read stats)
task split {
    File infile 
    
    command
    {
        shifter --image=bryce911/bbtools:38.44 \
            readlength.sh in=${infile} 1>| readlen.txt;

        shifter --image=bryce911/bbtools:38.44 \
            reformat.sh overwrite=true in=${infile} out=read1.fasta out2=read2.fasta
    }
    output {
        File readlen = "readlen.txt"
        File read1 = "read1.fasta"
        File read2 = "read2.fasta"
    }
}

# run MetaSpades
task spades {
    File read1
    File read2
    
    command
    {
        shifter --image=bryce911/spades:3.13.0  \
            spades.py -m 2000 -o spades3 --only-assembler -k 33,55,77,99,127 --meta -t 32 -1 ${read1} -2 ${read2}
    }
    output {
        File scaffolds = "spades3/scaffolds.fasta"
    }
    runtime {
        cpu: 32
        time: "24:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "annotation"
    }
}

# fungalrelease step will improve conteguity of assembly
task fungalrelease {
    File infile
    
    command
    {
        shifter --image=bryce911/bbtools:38.44 \
            fungalrelease.sh -Xmx10g in=${infile} out=assembly.scaffolds.fasta outc=assembly.contigs.fasta agp=assembly.agp legend=assembly.scaffolds.legend mincontig=200 minscaf=200 sortscaffolds=t sortcontigs=t overwrite=t
    }
    output {
        File assy = "assembly.scaffolds.fasta"
    }
}

# generate some assembly stats for the report
task stats {
    File infile
    
    command
    {
        shifter --image=bryce911/bbtools:38.44 \
            stats.sh format=6 in=${infile} 1> bbstats.tsv 2> bbstats.tsv.e 

        shifter --image=bryce911/bbtools:38.44 \
            stats.sh in=${infile} 1> bbstats.txt 2> bbstats.txt.e
    }
    output {
        File statstsv = "bbstats.tsv"
        File statstxt = "bbstats.txt"
    }
}

# Map the filtered (but not bfc corrected) reads back to scaffold
task bbmap {
    File fastq
    File ref
    
    command
    {
        shifter --image==bryce911/bbtools:38.44 \
            bbmap.sh nodisk=true interleaved=true ambiguous=random in=${fastq} ref=${ref} out=paired.mapped.bam covstats=paired.mapped.cov 

        shifter --image=jfroula/jgi_meta_annot:1.1.2 \
            samtools sort -@ 3 paired.mapped.bam -o paired.mapped_sorted.bam

        shifter --image=jfroula/jgi_meta_annot:1.1.2 \
            samtools index paired.mapped_sorted.bam
    }
    output {
        File bam = "paired.mapped.bam"
        File covstats = "paired.mapped.cov"
    }
}



task prodigal {
    File contigs

    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.2 prodigal -c -n -f gff -p meta -q \
              -i ${contigs} -o Prodigal.gff \
              -a Prodigal_proteins.faa -d Prodigal_genes.fna \
              -s Prodigal_starts.txt
    }
    output {
        File faa = "Prodigal_proteins.faa"
        File fna = "Prodigal_genes.fna"
        File gff = "Prodigal.gff"
        File starts = "Prodigal_starts.txt"
    }
    runtime {
        cluster: "cori"
        poolname: "small"
    }
}

task hmmer {
    File inproteins
    File data
    Int threads=1

    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.2 hmmsearch -o /dev/null --cpu ${threads} --domtblout Prodigal_Pfam.tbl \
            --notextw --cut_tc ${data}/Pfam-A.hmm ${inproteins} 

        shifter --image=jfroula/jgi_meta_annot:1.1.2 hmmer_tbl_gff Prodigal_Pfam.tbl | shifter --image=jfroula/jgi_meta_annot:1.1.2 filter_clans -c ${data}/Pfam_name_to_clan.tsv > Prodigal_Pfam.gff
    }
    output {
        File pfam_tbl = "Prodigal_Pfam.tbl"
        File pfam_gff = "Prodigal_Pfam.gff"
    }
    runtime { 
        cpu: 32
        time: "24:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "annotation"
    }

}

task cmsearch {
    File contigs
    File data
    Int threads = 64

    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.2 cmsearch --cpu ${threads} -o /dev/null --tblout Rfam.tbl --notextw \
            --cut_tc ${data}/Rfam.cm ${contigs} 

        shifter --image=jfroula/jgi_meta_annot:1.1.2 infernal_tbl_gff < Rfam.tbl | shifter --image=jfroula/jgi_meta_annot:1.1.2 filter_clans -c ${data}/Rfam_name_to_clan.tsv > Rfam.gff
    }
    output {
        File rfam_tbl = "Rfam.tbl"
        File rfam_gff = "Rfam.gff"
    }
    runtime {
        cpu: 32
        time: "24:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "annotation"
    }
}
