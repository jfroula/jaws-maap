# The Metagenome Assembly and Annotation Pipeline

## Summary
This takes fastq reads as input, assembles them with MetaSpades and then generates some annotations from the contigs.
This workflow runs some read filtering and error correction steps before running metaSpades.
Annotations are done using hmmscan & cmsearch to find pfams & rRNA/tRNAs. Some stats of read coverage of the assembly is also
given.

## Running Workflow in Cromwell
You should run this on cori.
It was tested on a cori exclusive node and uses 32 threads for some steps.

## The Docker image and Dockerfile can be found here
This workflow should have one docker image for it but instead has the following
```
bryce911/bbtools:38.44
jfroula/bfc:181
jfroula/jgi_meta_annot:1.1.2
bryce911/bbtools:38.44
bryce911/spades:3.13.0
jfroula/jgi_meta_annot:1.1.2
```
The dockerfiles may or may not be documented on https://hub.docker.com

## Running Requirements
unknown at this time

## Input files
expects: fastq, illumina, interleaved, paired-end, >130bp 

## Output files
```
```

## Dependency graph
![metagenome assembly & annotation workflow](workflow_20190503.png)
